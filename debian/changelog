projectm (3.1.12-4) unstable; urgency=medium

  [ Dylan Aïssi ]
  * Add pipewire-pulse as an alternative to pulseaudio

  [ Dennis Braun ]
  * Change my email address
  * Bump my d/copyright year
  * Clean build files (Closes: #1046094)
  * Build-Depends: libgl1-mesa-dev => libgl-dev
  * Add projectm-data as a depedency of projectm-jack (Closes: #1058583)

 -- Dennis Braun <snd@debian.org>  Wed, 10 Jan 2024 20:25:46 +0100

projectm (3.1.12-3) unstable; urgency=medium

  * Fix arm builds by adding the gl header to qprojectmwidget.hpp
  * Fix cross compiling. Thanks a lot to Helmut Grohne (Closes: #1022752)
  * Reformat all patches with gbp pq
  * Prefer libjack-dev over libjack-jackd2-dev
  * Apply wrap-and-sort -ast
  * d/copyright: Bump my entry
  * Add d/source/lintian-overrides
  * d/watch: Fix regex for github
  * Add salsa ci config

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 31 Oct 2022 13:35:02 +0100

projectm (3.1.12-2) unstable; urgency=medium

  * Bump Standards Version to 4.6.1
  * Switch projectm3 dependency from ttf-bitstream-vera to fonts-dejavu-core
    (Closes: #998268)
    + Update font patch: Use fonts-dejavu-core instead of ttf-bitstream-vera
  * Make projectm-pulseaudio depend on projectm-data (Closes: #997665)
  * Add libjack-jackd2-dev as B-D and libjack-dev as optional

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 06 Jun 2022 11:51:59 +0200

projectm (3.1.12-1) unstable; urgency=medium

  [ Dennis Braun ]
  * New upstream version 3.1.12
  * Refresh patchset, drop patches applied by upstream
  * Bump S-V to 4.5.1
  * Add d/source/lintian-overrides
  * Revert 'Remove README.md from new wrong path'

  [ Sebastian Ramacher ]
  * debian/control: Remove Breaks+Replaces post-bullseye

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 03 Oct 2021 22:23:27 +0200

projectm (3.1.7-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * projectm-data: Add Breaks+Replaces: libprojectm2 (Closes: #988171)

 -- Andreas Beckmann <anbe@debian.org>  Sun, 01 Aug 2021 21:42:23 +0200

projectm (3.1.7-1) unstable; urgency=medium

  * New upstream version 3.1.7
  * Update patchset
    + Provide keyboard controls in projectM-jack man page (Closes: #692582)
  * Remove README.md from new wrong path

 -- Dennis Braun <d_braun@kabelmail.de>  Sat, 01 Aug 2020 23:02:11 +0200

projectm (3.1.3-2) unstable; urgency=medium

  * Update Breaks and Replaces for projectm-data (Closes: #963726)
  * Add X-AppStream-Ignore=true to the desktop files
  * Remove unused ${shlibs:Depends} from libprojectm-dev
  * Add proper pthread linking

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 29 Jun 2020 19:15:01 +0200

projectm (3.1.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Matt Beary ]
  * update link to upstream
  * update watch file to the new github location
  * correct font paths in build script

  [ Dennis Braun ]
  * New upstream version 3.1.3
    + Win and Mac modules are removed by upstream
  * Remove obsolete qt and libvisual packages
  * Remove obsolete debian files
  * Enable OpenGL ES support
    + Add projectm-sdl as package
  * Bump SO version
  * d/control:
    + Use dh-compat instead of d/compat and bump to 13
    + Update B-D and Depends
    + Bump S-V to 4.5.0
    + Add me as uploader
    + Set RRR: no
    + Set M-A: Foreign for projectm-data
    + Set M-A: Same for libprojectm3 & libprojectm-dev
  * Update d/copyright file
  * Update install files
  * Add d/not-installed
  * Add new patchset
  * Update d/rules
    + Add hardening
    + Fix installation
  * Add d/upstream/metadata
  * Update d/watch file

 -- Dennis Braun <d_braun@kabelmail.de>  Thu, 07 May 2020 23:01:38 +0200

projectm (2.1.0+dfsg-4) unstable; urgency=medium

  * Acknowledge NMU
  * fix-gcc6-build.patch: Fix FTBFS with GCC 6 (Closes: #811831)
  * Stop building debug package: We have automatic dbgsym packages now
  * Bump standards version: No changes needed
  * Use secure Vcs-* URLs
  * Modernize gbp.conf

 -- Matthias Klumpp <mak@debian.org>  Thu, 28 Jul 2016 22:24:56 +0200

projectm (2.1.0+dfsg-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use fonts-dejavu-core instead of ttf-dejavu-core (Closes: #733957)

 -- Adam Borowski <kilobyte@angband.pl>  Sat, 30 Apr 2016 04:19:39 +0200

projectm (2.1.0+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Aug 2015 00:25:42 +0200

projectm (2.1.0+dfsg-2) experimental; urgency=medium

  * Team upload.

  [ Matthias Klumpp ]
  * Fix debian/watch file
  * Use my Debian mail address
  * Bump standards version (no change needed)

  [ Sebastian Ramacher ]
  * Rename library packages for GCC 5 transition.
  * debian/copyright: Fix lintian warnings.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 02 Aug 2015 18:47:32 +0200

projectm (2.1.0+dfsg-1) unstable; urgency=low

  * New upstream release: 2.1.0
    - Fixed several memory leaks (when projectM gets destroyed,
       or when a preset gets destroyed)
    - Resolved some parser errors and an embarrasing evaluation bug
       (in particular, 2^x does not equal x^2)
    - +/- keys support to edit the ratings
    - libvisual / gstreamer compatibility
    - Various cmake build fixes and improvements
    - Patches for better Linux distribution integration
  * Switch to compat-level 9
  * Update dfsg-free projectM generator script
  * Drop all patches: Applied upstream
  * Install libvisual-plugin into multiarch dir
  * Set font-settings as build-time parameters
  * Make debian/copyright DEP-5 compliant

 -- Matthias Klumpp <matthias@tenstral.net>  Mon, 21 May 2012 14:25:49 +0200

projectm (2.0.1+dfsg-12) unstable; urgency=low

  * Make parser handle cases like "1.0-1.0-2.0"
  * Fix wrong sqr and sigmoid functions
  * Unbind FBO when done (might fix some OGL errors)

 -- Matthias Klumpp <matthias@tenstral.net>  Sun, 01 Jan 2012 18:31:08 +0100

projectm (2.0.1+dfsg-11) unstable; urgency=low

  [ Reinhard Tartler ]
  * use Breaks: libprojectm-data instead Conflicts

  [ Matthias Klumpp ]
  * Fix FTBFS with recent PulseAudio versions (Closes: #646492)
  * Adjust path to projectM header (Closes: #642498)
  * Allow DM upload
  * Fix FTBFS on multiarch Debian config (files were placed wrong)

 -- Matthias Klumpp <matthias@tenstral.net>  Sun, 20 Nov 2011 21:14:58 +0100

projectm (2.0.1+dfsg-10) unstable; urgency=low

  * Use DejaVu font instead of Bistream-Vera (Closes: #630557)
  * Update my email-address
  * Update projectM-Qt dev package dependencies
  * Fix projectM linking (LP: #819025)
  * Rebuild for libglew1.5 -> libglew1.6 transition

 -- Matthias Klumpp <matthias@tenstral.net>  Mon, 01 Aug 2011 19:40:08 +0200

projectm (2.0.1+dfsg-9) unstable; urgency=low

  * Fix incorrect token parsing in presetfactory
    projectM-pulseaudio hangs whilst trying to parse
    the idle preset due to incorrect token parsing in
    libprojectm2 (Closes: #627616).
    Thanks to Adam Guthrie for the patch!
  * Adjusted VCS links

 -- Matthias Klumpp <matthias@nlinux.org>  Thu, 09 Jun 2011 13:38:29 +0200

projectm (2.0.1+dfsg-8) unstable; urgency=low

  * Team upload.
  * Add patch to fix FTBFS with GCC4.6 (Closes: #624898, LP: #771045).
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Fri, 06 May 2011 14:24:11 +0200

projectm (2.0.1+dfsg-7) unstable; urgency=low

  [ Matthias Klumpp ]
  * Bump debhelper version dependency
  * Make more use of dh-auto commands
  * Patch projectM cmake script to find FreeType
  * Set LC_NUMERIC to "C" in libprojectM (LP: #737915)

  [ Alessio Treglia ]
  * Correct team's name.

  [ Reinhard Tartler ]
  * add myself to uploaders
  * normalize fields with wrap-and-sort(1)
  * apply best practices for dpkg source format 3.0 (quilt)

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 23 Mar 2011 20:38:38 +0100

projectm (2.0.1+dfsg-6) experimental; urgency=low

  * Added some missing setter methods for Clementine (Closes: #600653)

 -- Matthias Klumpp <matthias@nlinux.org>  Fri, 19 Nov 2010 20:30:12 +0100

projectm (2.0.1+dfsg-5) unstable; urgency=low

  * Added patches to make projectM find the right fonts
  * Do not crash if fonts are not found
  * Re-enabled FTGL support

 -- Matthias Klumpp <matthias@nlinux.org>  Tue, 28 Sep 2010 18:50:15 +0200

projectm (2.0.1+dfsg-4) unstable; urgency=low

  * Disable FTGL support (LP: #634925)
    (caused projectM to crash on some machines)

 -- Matthias Klumpp <matthias@nlinux.org>  Sat, 18 Sep 2010 19:02:50 +0200

projectm (2.0.1+dfsg-3) unstable; urgency=low

  * Generate valid pc file for libprojectM (Closes: #591011)
  * Update debian/watch file
  * Replaced section 'libs' with 'sound' in libvisual-projectm
    (Solves override disparity)

 -- Matthias Klumpp <matthias@nlinux.org>  Sat, 31 Jul 2010 18:41:14 +0200

projectm (2.0.1+dfsg-2) unstable; urgency=low

  * Prevent projectM-pulseaudio from crashing if graphics
    are not supported (Closes: #590706)
  * Check if PulseAudio is running in projectM-pulseaudio
  * Improve projectM error handling
  * Replace libprojectm-data with projectm-data
  * Bump standards version to 3.9.1

 -- Matthias Klumpp <matthias@nlinux.org>  Thu, 29 Jul 2010 18:11:52 +0200

projectm (2.0.1+dfsg-1) experimental; urgency=low

  * Initial release (Closes: #565355, #588529)

 -- Matthias Klumpp <matthias@nlinux.org>  Thu, 01 Jul 2010 14:58:41 +0200
