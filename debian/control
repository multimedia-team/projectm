Source: projectm
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Matthias Klumpp <mak@debian.org>,
 Reinhard Tartler <siretart@tauware.de>,
 Dennis Braun <snd@debian.org>,
Vcs-Git: https://salsa.debian.org/multimedia-team/projectm.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/projectm
Build-Depends:
 autoconf-archive (>= 20220903-2),
 debhelper-compat (= 13),
 libfreetype6-dev,
 libftgl-dev,
 libgl-dev,
 libgles-dev,
 libglew-dev,
 libglm-dev,
 libglu-dev,
 libice-dev,
 libjack-dev | libjack-jackd2-dev,
 libpulse-dev,
 libqt5opengl5-dev,
 libsdl2-dev,
 libvisual-0.4-dev,
 libx11-dev,
 qtbase5-dev,
Standards-Version: 4.6.1
Homepage: https://github.com/projectM-visualizer/projectm
Rules-Requires-Root: no

Package: libprojectm3
Architecture: any
Multi-Arch: same
Section: libs
Depends:
 fonts-dejavu-core,
 projectm-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Advanced Milkdrop-compatible music visualization library
 libprojectm is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains the library and essential data needed for
 the visualization library to work.

Package: libprojectm-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libprojectm3 (= ${binary:Version}),
 ${misc:Depends},
Description: Advanced Milkdrop-compatible music visualization library - dev
 libprojectm is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains the development headers.

Package: projectm-data
Architecture: all
Multi-Arch: foreign
Section: libs
Depends:
 ${misc:Depends},
Description: Advanced Milkdrop-compatible music visualization library - data
 libprojectm is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains the presets which are distributed with
 projectM.

Package: projectm-jack
Architecture: any
Depends:
 jackd,
 projectm-data,
 ${misc:Depends},
 ${shlibs:Depends},
Description: projectM JackAudio module
 projectM is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains an application to visualize JackAudio streams.

Package: projectm-pulseaudio
Architecture: any
Depends:
 projectm-data,
 pulseaudio | pipewire-pulse,
 ${misc:Depends},
 ${shlibs:Depends},
Description: projectM PulseAudio module
 projectM is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains an application to visualize PulseAudio streams.

Package: projectm-sdl
Architecture: any
Depends:
 projectm-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: projectM SDL module
 projectm is an iterative music visualization library which uses
 OpenGL for hardware acceleration. It is compatible with Milkdrop
 presets.
 .
 This package contains the SDL module which is distributed with
 projectM.
